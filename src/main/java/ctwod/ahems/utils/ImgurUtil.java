package ctwod.ahems.utils;

import com.fasterxml.jackson.databind.ObjectMapper;
import ctwod.ahems.modells.DataModel;
import ctwod.ahems.modells.ImgurGalleryModel;
import ctwod.ahems.modells.ImgurImageModel;
import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by Alex on 28.02.2016.
 */
public class ImgurUtil {

    private ImgurUtil() {
    }

    public static String getImgurGallery() throws IOException {
        ObjectMapper objectMapper = new ObjectMapper();
        HttpURLConnection httpURLConnection = (HttpURLConnection) new URL("https://api.imgur.com/3/gallery/NcHpG").openConnection();
        httpURLConnection.setRequestProperty("Authorization","Client-ID 52c6e89eac4bb0f");
        DataModel dataModel = objectMapper.readValue(httpURLConnection.getInputStream(), DataModel.class);
        ImgurGalleryModel data = dataModel.getData();
        ImgurImageModel[] images = data.getImages();
        int i = (int) (Math.random() * 229);
        return images[i].getLink();

    }

    public static ImgurGalleryModel getImgurGalleryArray() throws IOException {
        ObjectMapper objectMapper = new ObjectMapper();
        HttpURLConnection httpURLConnection = (HttpURLConnection) new URL("https://api.imgur.com/3/gallery/0ngOS").openConnection();
        httpURLConnection.setRequestProperty("Authorization","Client-ID 52c6e89eac4bb0f");
        DataModel dataModel = objectMapper.readValue(httpURLConnection.getInputStream(), DataModel.class);
        ImgurGalleryModel data = dataModel.getData();
        return data;
    }

    public static void saveGallery(){
        try {
            ImgurGalleryModel imgurGalleryArray = getImgurGalleryArray();
            String prefix = imgurGalleryArray.getId()+"\\";
            File imageDir = new File(imgurGalleryArray.getId());
            if(imageDir.exists() && imageDir.mkdir()){
                System.out.println("Folder ist created");
            }
            for (ImgurImageModel image : imgurGalleryArray.getImages()) {
                FileUtils.copyURLToFile(new URL(image.getLink()),new File(prefix+image.getId()+"."+image.getType().split("/")[1]));
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


}
