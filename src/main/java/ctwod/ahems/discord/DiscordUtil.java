package ctwod.ahems.discord;

import ctwod.ahems.Main;
import ctwod.ahems.listener.AnnotationListener;
import sx.blah.discord.api.ClientBuilder;
import sx.blah.discord.api.EventDispatcher;
import sx.blah.discord.api.IDiscordClient;
import sx.blah.discord.util.DiscordException;
import sx.blah.discord.util.HTTP429Exception;
import sx.blah.discord.util.Image;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Optional;
import java.util.Properties;

/**
 * Created by ahems on 28.02.16.
 */
public class DiscordUtil {

    private static IDiscordClient client;

    public static IDiscordClient getClient() {
        return client;
    }

    public static Properties props;

    public static void setClient(IDiscordClient client) {
        DiscordUtil.client = client;
    }

    public static IDiscordClient getClient(String token ,boolean login) throws DiscordException { //Returns an instance of the discord client
        ClientBuilder clientBuilder = new ClientBuilder(); //Creates the ClientBuilder instance
        clientBuilder.withToken(token); // Set your Token here - change to configuration.properties immediately
        if (login) {
            return clientBuilder.login(); //Creates the client instance and logs the client in
        } else {
            return clientBuilder.build(); //Creates the client instance but it doesn't log the client in yet, you would have to call client.login() yourself
        }
    }

    public static Boolean logout(){
        Boolean status = true;
        if(client != null && client.isReady()){
            try {
                client.logout();
            } catch (HTTP429Exception e) {
                status = false;
                e.printStackTrace();
            } catch (DiscordException e) {
                status = false;
                e.printStackTrace();
            }
        }
        return status;
    }

    public static void login(String token){

        try {
            client = getClient(token, true);
        } catch (DiscordException e) {
            System.out.println("Failed to Login!\n"+e.getMessage());
        }
        if(client != null){
            EventDispatcher dispatcher = client.getDispatcher(); //Gets the EventDispatcher instance for this client instance
            dispatcher.registerListener(new AnnotationListener()); //Registers the @EventSubscriber example class from above
            Main.updateLog(" : Bot Login successful!");
        }
    }

    public static boolean changeStatus(String statusText){
        boolean success = false;
        if(client != null){
            client.updatePresence(false, Optional.of(statusText));
            success = true;
        }
        return success;
    }

    public static boolean changeAccountInfo(String username,String email, String password, Image avatar){
        boolean success = false;
        if(client != null){
            try {
                if(!username.isEmpty()) client.changeUsername(username);
                if(!email.isEmpty()) client.changeEmail(email);
                if(!password.isEmpty()) client.changePassword(password);
                if(avatar != null) client.changeAvatar(avatar);
                success = true;
            } catch (HTTP429Exception e) {
                e.printStackTrace();
            } catch (DiscordException e) {
                e.printStackTrace();
            }
        }
        return success;
    }

    public static Properties getProps() {
        return props;
    }

    public static void setProps(Properties props) {
        DiscordUtil.props = props;
    }
}
