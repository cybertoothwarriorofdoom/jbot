package ctwod.ahems;

import ctwod.ahems.controller.MainController;
import ctwod.ahems.discord.DiscordUtil;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;

import java.io.IOException;

public class Main extends Application {

    private static Stage primaryStage;
    private BorderPane loginScreen;
    private BorderPane mainScreen;
    private static Scene loginScene;
    private static Scene mainScene;
    private static MainController mainCtlr;



    public static void main(String[] args) {
        launch(args);
    }



    public Stage getPrimaryStage() {
        return primaryStage;
    }

    public static MainController getMainCtlr() {
        return mainCtlr;
    }

    public static void setMainCtlr(MainController mainCtlr) {
        Main.mainCtlr = mainCtlr;
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        this.primaryStage = primaryStage;
        this.primaryStage.setTitle("JBot");

        initRootLayout();
        initMainScreen();
        primaryStage.setOnCloseRequest(new EventHandler<WindowEvent>() {
            public void handle(WindowEvent we) {
                System.out.println("Stage is closing");
                DiscordUtil.logout();
                Platform.exit();
                System.exit(0);
            }
        });
    }

    public static void changeScreen(){
       primaryStage.setScene(mainScene);
        primaryStage.show();
    }

    public static void updateLog(String logString){
        mainCtlr.updateLog(logString);
    }


    public void initMainScreen(){
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getClassLoader().getResource("views/main.fxml"));
        try {
            mainScreen =  loader.load();
            mainScene = new Scene(mainScreen);
            mainCtlr = loader.getController();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void initRootLayout() {
        try {
            // Load root layout from fxml file.
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(getClass().getClassLoader().getResource("views/login.fxml"));
            loginScreen = (BorderPane) loader.load();

            // Show the scene containing the root layout.
            loginScene = new Scene(loginScreen);
            primaryStage.setScene(loginScene);
            primaryStage.show();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
