package ctwod.ahems.controller;

import ctwod.ahems.Main;
import ctwod.ahems.discord.DiscordUtil;
import javafx.fxml.FXML;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Map;
import java.util.Properties;

/**
 * Created by ahems on 25.02.16.
 */
public class LoginController {

    @FXML
    private TextField userName;

    @FXML
    private PasswordField password;

    @FXML
    private MainController mainController;


    @FXML
    public void loginDiscord(){
        Properties props = DiscordUtil.getProps();
        if(props == null){
            props = setProperties(props);
            DiscordUtil.setProps(props);
        }
        if(props != null){
            String token = props.getProperty("token");
            if(token != null && !token.isEmpty()){
                DiscordUtil.login(token);
                Main.changeScreen();
            } else {
                System.out.println("Token is empty pls set Token in config.properties");
            }
        }else{
            System.out.println("cant find config.properties");
        }




//        String user = userName.getText();
//        String pwd = password.getText();




    }

    private Properties setProperties(Properties props){
        if(props == null){
            props = new Properties();
            try {
                ClassLoader classLoader = getClass().getClassLoader();
                props.load(new FileInputStream(classLoader.getResource("config.properties").getFile()));
            } catch (IOException e) {
                System.out.println("Error by loading config.properties");
            }
        }
        return props;
    }

}
