package ctwod.ahems.controller;

import ctwod.ahems.Main;
import ctwod.ahems.discord.DiscordUtil;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.URL;
import java.util.Date;
import java.util.ResourceBundle;

/**
 * Created by ahems on 26.02.16.
 */
public class MainController implements Initializable {

    private static final Logger LOG = LoggerFactory.getLogger(MainController.class);

    @FXML
    TextArea log;

    @FXML
    TextField updateStatus;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        Main.setMainCtlr(this);
    }


    public void updateLog(String logString){
        String text = log.getText();
        if(text.isEmpty()){
            text = "";
        } else {
            text = text + "\n";
        }
        String logText = new Date(System.currentTimeMillis()).toString() + logString;
        text = text + logText;
        log.setText(text);
        LOG.info(logString);
    }

    @FXML
    public void clearLog(){
        log.setText("");
        log.setText("Log Cleared");
    }

    @FXML
    public void changeStatus(){
        String text = updateStatus.getText();
        if(!text.isEmpty()){
            boolean b = DiscordUtil.changeStatus(text);
            updateLog(" : Text change for "+text+" was "+(b ? "successful" : "not successful"));
        } else {
            updateLog(" : No Text found for Change");
        }
    }


}
