package ctwod.ahems.modells;

/**
 * Created by Alex on 28.02.2016.
 */
public class DataModel {

    private ImgurGalleryModel data;
    private boolean success;
    private String status;

    public ImgurGalleryModel getData() {
        return data;
    }

    public void setData(ImgurGalleryModel data) {
        this.data = data;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
