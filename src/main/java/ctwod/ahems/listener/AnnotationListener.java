package ctwod.ahems.listener;

import ctwod.ahems.commands.ImageCommands;
import ctwod.ahems.commands.MathCommands;
import ctwod.ahems.commands.YoutubeCommands;
import sx.blah.discord.api.EventSubscriber;
import sx.blah.discord.api.IDiscordClient;
import sx.blah.discord.handle.impl.events.MentionEvent;
import sx.blah.discord.handle.impl.events.ReadyEvent;
import sx.blah.discord.handle.obj.IMessage;

/**
 * Created by ahems on 24.02.16.
 */
public class AnnotationListener {

    @EventSubscriber
    public void onReadyEvent(ReadyEvent event) { //This method is called when the ReadyEvent is dispatched
        System.out.println("I am RDY todo Stuff now <3");
//        List<IGuild> guilds = event.getClient().getGuilds();
//        try {
//            for (IGuild guild : guilds){
//                if(guild.getName().equalsIgnoreCase("poidechikuma")){
//                    event.getClient().getChannelByID(guild.getID()).sendMessage("PRAISE THE SUN");
//                }
//            }
//        } catch (MissingPermissionsException e) {
//            e.printStackTrace();
//        } catch (HTTP429Exception e) {
//            e.printStackTrace();
//        } catch (DiscordException e) {
//            e.printStackTrace();
//        }
    }

    @EventSubscriber
    public void onMention(MentionEvent event) {
        IMessage message = event.getMessage();
        String botId = event.getClient().getOurUser().getID();
        IDiscordClient client = event.getClient().getOurUser().getClient();
        String content = message.getContent();
        content = content.replace("<@" + botId + ">", "").trim();
        String[] split = content.split(" ");
        content = split[0];
        switch (content){
            case "random":
                MathCommands.sendRandomNumber(message);
                break;
            case "spoderman":
                ImageCommands.sendRandomSpodermanMeme(message);
                break;
            case "youtube":
                if(split.length > 1){
                    if(split[1].matches("https:\\/\\/www\\.youtube\\.com\\/watch\\?v=.*"))
                    YoutubeCommands.playYoutube(message,client,split[1].split("=")[1]);
                }
            case "stop":
                YoutubeCommands.stop(client);
            default:

                break;
        }


    }
// DEACTIVATED FOR A GOOD REASON!!!!
//    @EventSubscriber
//    public void onPresenceChange(PresenceUpdateEvent event){
//        IUser user = event.getUser();
//        if(event.getNewPresence().name().equals(Presences.ONLINE.name())){
//            IGuild guild = event.getGuild();
//            String generalID = guild.getID();
//            IDiscordClient client = event.getClient();
//            IChannel channelByID = client.getChannelByID(generalID);
//            try {
//                channelByID.sendMessage(user.mention() + " PRAISE THE SUN!!");
//            } catch (MissingPermissionsException e) {
//                e.printStackTrace();
//            } catch (HTTP429Exception e) {
//                e.printStackTrace();
//            } catch (DiscordException e) {
//                e.printStackTrace();
//            }
//        }
//    }

}
