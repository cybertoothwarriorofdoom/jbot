package ctwod.ahems.commands;

import ctwod.ahems.Main;
import sx.blah.discord.handle.obj.IMessage;
import sx.blah.discord.util.DiscordException;
import sx.blah.discord.util.HTTP429Exception;
import sx.blah.discord.util.MissingPermissionsException;

/**
 * Created by ahems on 28.02.16.
 */
public class MathCommands {

    public static void sendRandomNumber(IMessage message){
        try {
            message.reply(""+(int)(Math.random()*1000));
            Main.updateLog(" : Random Number Command for User : "+message.getAuthor().getName());
        } catch (MissingPermissionsException e) {
            Main.updateLog(" : " + e.getMessage());
        } catch (HTTP429Exception e) {
            Main.updateLog(" : " + e.getMessage());
        } catch (DiscordException e) {
            Main.updateLog(" : " + e.getMessage());
        }

    }


}
