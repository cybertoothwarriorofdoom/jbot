package ctwod.ahems.commands;

import ctwod.ahems.Main;
import ctwod.ahems.utils.ImgurUtil;
import sx.blah.discord.handle.obj.IMessage;
import sx.blah.discord.util.DiscordException;
import sx.blah.discord.util.HTTP429Exception;
import sx.blah.discord.util.MissingPermissionsException;

import java.io.IOException;

/**
 * Created by ahems on 28.02.16.
 */
public class ImageCommands {

    public static void sendRandomSpodermanMeme(IMessage message){
        try {

            message.reply(ImgurUtil.getImgurGallery());
            Main.updateLog(" : Spoderman Command for User : "+message.getAuthor().getName());
        } catch (MissingPermissionsException e) {
            Main.updateLog(" : " + e.getMessage());
        } catch (HTTP429Exception e) {
            Main.updateLog(" : " + e.getMessage());
        } catch (DiscordException e) {
            Main.updateLog(" : " + e.getMessage());
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

}
