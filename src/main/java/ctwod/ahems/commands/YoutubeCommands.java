package ctwod.ahems.commands;


import ctwod.ahems.Main;
import org.omg.SendingContext.RunTime;
import sx.blah.discord.api.IDiscordClient;
import sx.blah.discord.handle.AudioChannel;
import sx.blah.discord.handle.obj.IMessage;
import sx.blah.discord.handle.obj.IVoiceChannel;
import sx.blah.discord.util.DiscordException;
import sx.blah.discord.util.HTTP429Exception;
import sx.blah.discord.util.MissingPermissionsException;

import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioInputStream;
import java.io.*;
import java.util.List;
import java.util.Optional;

/**
 * Created by Alex on 31.05.2016.
 */
public class YoutubeCommands {

    public static void playYoutube(IMessage message, IDiscordClient client, String ytID){
        try {
            Process exec = null;
            File mp3 = new File(ytID+".mp3");
            File webm = new File(ytID+".webm");
            if(!webm.exists() && !mp3.exists()) {
                message.reply("Download and convert your Youtubelink");
                exec = Runtime.getRuntime().exec("youtube-dl -x --audio-format mp3 --audio-quality 4 --output %(id)s.%(ext)s https://www.youtube.com/watch?v="+ytID);
                while (exec.isAlive()) {
                }
            }
            if(!mp3.exists()){
                exec  = Runtime.getRuntime().exec("ffmpeg -i " + webm.getAbsolutePath() + " -vn -ab 128k -ar 44100 -y " + webm.getAbsolutePath().replace(".webm", ".mp3"));
                BufferedReader input = new BufferedReader(new InputStreamReader(exec.getInputStream()));
                String s = "";
                while(exec.isAlive() && (s = input.readLine()) != null){
                    System.out.println(s);
                }
            }
            if(!webm.exists()){
                boolean delete = webm.delete();
                System.out.println("Delete old file");
            }
            Optional<IVoiceChannel> voiceChannel = message.getAuthor().getVoiceChannel();
                if(voiceChannel.isPresent() && !voiceChannel.get().isConnected()){
                    voiceChannel.get().join();
                }
                message.reply("Now i play your song :D");
                try {
                    AudioChannel audioChannel = message.getGuild().getAudioChannel();
                    audioChannel.skip();
                    audioChannel.resume();
                    audioChannel.queueFile(mp3);
                } catch (DiscordException e) {
                    Main.updateLog(" : " + e.getMessage());
                }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (DiscordException e) {
            e.printStackTrace();
        } catch (HTTP429Exception e) {
            e.printStackTrace();
        } catch (MissingPermissionsException e) {
            e.printStackTrace();
        }

    }

    public static void stop(IDiscordClient client){
        List<IVoiceChannel> connectedVoiceChannels = client.getConnectedVoiceChannels();
        for(IVoiceChannel channel : connectedVoiceChannels){
            try {
                channel.getAudioChannel().pause();
                channel.leave();
            } catch (DiscordException e) {
                e.printStackTrace();
            }
        }
    }

    public static void clearQueue(IDiscordClient client){
        List<IVoiceChannel> connectedVoiceChannels = client.getConnectedVoiceChannels();
        for(IVoiceChannel channel : connectedVoiceChannels){
            try {
                channel.getAudioChannel().clearQueue();
            } catch (DiscordException e) {
                e.printStackTrace();
            }
        }
    }

    public static void play(IDiscordClient client){
        List<IVoiceChannel> connectedVoiceChannels = client.getConnectedVoiceChannels();
        for(IVoiceChannel channel : connectedVoiceChannels){
            try {
                channel.getAudioChannel().resume();
            } catch (DiscordException e) {
                e.printStackTrace();
            }
        }
    }

}
